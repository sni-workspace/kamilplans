import React from 'react';
import {SafeAreaView, Text} from 'react-native';

export default class App extends React.PureComponent {
  render() {
    return (
      <SafeAreaView style={styles.main}>
        <Text style={styles.mainText}>Hejka naklejka</Text>
      </SafeAreaView>
    );
  }
}

const styles = {
  main: {
    backgroundColor: '#ffa500',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
  },
  mainText: {
    fontSize: 50,
    fontWeight: '900',
  },
};
